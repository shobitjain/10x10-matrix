# 10x10 Matrix #

1. This algorithm loads words from the file into a 10x10 matrix. 
2. It takes a file as an argument and the output shown in the console.
3. If the file doesn't exist or empty or more than 100 characters then It would show a correspondence error.
4. Empty characters replaced by the '+' sign.

Assumption: 
Space count as a character

# More Details is in doc file #