#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[])
{
    char charArray[100];
    FILE *fp1;
    int i, j, k = 0;
    int final_array[10][10];

    for (i = 0; i < 100; i++)
        charArray[i] = '\0';
    i = 0;
    if ((fp1 = fopen(argv[1], "r")) == NULL)
    {
        printf("ERROR: %s doesn't exist! Enter correct filename\n", argv[1]);
        return 1;
    }
    else if ((fscanf(fp1, "%c", &charArray[0])) == EOF)
    {
        printf("ERROR: %s is empty\n", argv[1]);
        return 1;
    }

    else
    {
        rewind(fp1);
        while ((fscanf(fp1, "%c", &charArray[i])) != EOF)
        {
            if (charArray[i] != '\n')
            {
                i++;
            }
            if (i == 101)
            {
                printf("ERROR: %s is too long\n", argv[1]);
                return 1;
            }
        }
    }
    for (i = 9; i >= 0; i--)
    {
        for (j = 0; j < 10; j++)
        {
            final_array[i][j] = charArray[k];
            if (!charArray[k])
                final_array[i][j] = '+';
            k++;
            //printf("final_array[%d][%d] is %c\n",i,j,final_array[i][j]);
        }
        //printf("\n");
    }

    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            printf("%c", final_array[i][j]);
        }
        printf("\n");
    }
    return 0;
}
